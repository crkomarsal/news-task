import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import ArticleCard from "./ArticleCard";
import { Grid, Button, GridRow } from "semantic-ui-react";

export function ArticlesList({
  articles,
  loadMoreArticles,
  onArticleClick,
  totalResults,
}) {
  const [pageSize, setPageSize] = useState(20);

  function onLoadMoreArticles() {
    setPageSize(pageSize + 20);
  }

  useEffect(() => {
    loadMoreArticles(pageSize);
  }, [pageSize]);

  return (
    <Grid columns={4} container stackable>
      {articles.map((article, i) => {
        return (
          <Grid.Column key={`${article.source.id || article.source.name}-${i}`}>
            <ArticleCard article={article} onArticleClick={onArticleClick} />
          </Grid.Column>
        );
      })}
      <GridRow>
        {totalResults > articles.length && (
          <Button onClick={onLoadMoreArticles} fluid>
            Load More
          </Button>
        )}
      </GridRow>
    </Grid>
  );
}

ArticlesList.propTypes = {
  articles: PropTypes.array.isRequired,
  onArticleClick: PropTypes.func.isRequired,
  loadMoreArticles: PropTypes.func.isRequired,
  totalResults: PropTypes.number.isRequired,
};

export default ArticlesList;
