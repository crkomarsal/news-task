import React from "react";
import { NavLink } from "react-router-dom";
import { Menu } from "semantic-ui-react";

const Header = () => {
  return (
    <Menu style={{ alignItems: "center" }}>
      <NavLink to="/" exact>
        <img
          style={{ width: "auto", height: 60 }}
          src="https://waltercode.com/wp-content/uploads/2019/11/walter-code-logo.png"
        />
      </NavLink>

      <NavLink to="/search" style={{ marginLeft: 30 }}>
        Search
      </NavLink>
    </Menu>
  );
};

export default Header;
