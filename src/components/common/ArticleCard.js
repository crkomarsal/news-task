import React from "react";
import PropTypes from "prop-types";
import { Card, Icon, Image } from "semantic-ui-react";

const ArticlesCard = ({ article, onArticleClick }) => (
  <Card style={{ margin: "auto" }} onClick={onArticleClick.bind(this, article)}>
    <Image
      src={
        article.urlToImage
          ? article.urlToImage
          : "https://www.mylittleadventure.com/images/default/default-img.png"
      }
      wrapped
      ui={true}
    />
    <Card.Content>
      <Card.Header>
        {article.title.length > 50
          ? `${article.title.substring(0, 50)}...`
          : article.title}
      </Card.Header>
      <Card.Meta>
        <span className="date">{`${new Date(
          article.publishedAt
        ).toLocaleDateString()}`}</span>
      </Card.Meta>
      <Card.Description>
        {article.description
          ? article.title.substring(0, 105)
          : "Description  is not available..."}
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      <a>
        <Icon name="at" />
        {article.source.name}
      </a>
    </Card.Content>
  </Card>
);

ArticlesCard.propTypes = {
  article: PropTypes.object.isRequired,
  onArticleClick: PropTypes.func.isRequired,
};

export default ArticlesCard;
