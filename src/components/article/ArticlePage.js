import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Title from "../common/Title";
import { Col, Image } from "react-bootstrap";
import PropTypes from "prop-types";
import { Container, Grid } from "semantic-ui-react";

export function ArticlePage({ article }) {
  return !article.source ? (
    <Redirect to={"/"} />
  ) : (
    <Container text>
      <Title title={article.title} />
      {article.author && (
        <Grid.Column>
          <p>
            <strong>{`By ${article.author}`}</strong>
          </p>
        </Grid.Column>
      )}
      {article.publishedAt && (
        <Grid.Column>
          <p>{`${new Date(article.publishedAt).toLocaleDateString()}`}</p>
        </Grid.Column>
      )}
      {article.urlToImage && (
        <Grid.Column>
          <Image
            src={article.urlToImage}
            style={{ width: "100%" }}
            rounded
            wrapped
            ui={true}
          />
        </Grid.Column>
      )}
      {article.description && (
        <Grid.Column>
          <h3>
        {article.description}
          </h3>
        </Grid.Column>
      )}
      {article.content && (
        <Grid.Column>
          <p>{article.content}</p>
        </Grid.Column>
      )}
    </Container>
  );
}

ArticlePage.propTypes = {
  article: PropTypes.object.isRequired,
};

function mapStateToProps({ article }) {
  return {
    article,
  };
}

export default connect(mapStateToProps, {})(ArticlePage);
