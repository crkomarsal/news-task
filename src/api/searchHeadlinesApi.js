import { handleResponse, handleError } from "./apiUtils";
const baseUrl =
  "https://newsapi.org/v2/everything";
const apiKey = "f037387b5d2a493ea0e32b77fbb6eb32";
export function searchHeadlines(searchTerm, pageSize, sortBy) {
  return fetch(
    `${baseUrl}?q=${searchTerm}&pageSize=${pageSize}&sortBy=${sortBy}&apiKey=${apiKey}`
  )
    .then(handleResponse)
    .catch(handleError);
}
