import { handleResponse, handleError } from "./apiUtils";
const baseUrl =
  "https://newsapi.org/v2/top-headlines";
const apiKey = "f037387b5d2a493ea0e32b77fbb6eb32";
export function getTopHeadlines(pageSize) {
  return fetch(`${baseUrl}?country=us&pageSize=${pageSize}&apiKey=${apiKey}`)
    .then(handleResponse)
    .catch(handleError);
}
